import {
  arrayOf, shape, string, number, bool,
} from 'prop-types';

export const priorityShape = shape({
  name: string.isRequired,
});

export const projectShape = shape({
  name: string.isRequired,
});

export const selectOptionShape = shape({
  label: string.isRequired,
  value: string.isRequired,
});

export const selectOptionsShape = arrayOf(selectOptionShape);

export const taskShape = shape({
  id: string.isRequired,
  date: number.isRequired,
  name: string.isRequired,
  priorityId: string.isRequired,
  projectId: string.isRequired,
  time: number,
  done: bool,
});

export const taskExtendedShape = shape({
  id: string.isRequired,
  date: number.isRequired,
  name: string.isRequired,
  priorityId: string.isRequired,
  priority: selectOptionShape,
  projectId: string.isRequired,
  project: selectOptionShape,
  time: number,
  done: bool,
});
