import axios from 'axios';
import {
  onRequestInterceptor,
  onResponseInterceptor,
} from 'api/interceptors';

const projectId = 'tesk-tracking';

const axiosConfig = {
  baseURL: `https://${projectId}.firebaseio.com`,
  headers: { 'Content-Type': 'application/json;charset=UTF-8' },
};

export const axiosApi = axios.create(axiosConfig);

axiosApi.interceptors.request.use(onRequestInterceptor);
axiosApi.interceptors.response.use(onResponseInterceptor);

export default axiosApi;
