/* eslint-disable import/prefer-default-export */
import { axiosApi } from 'api/config';

export const getPriorities = () => axiosApi({
  method: 'GET',
  url: '/priority.json',
});
