/* eslint-disable import/prefer-default-export */
import { axiosApi } from 'api/config';

export const getProjects = () => axiosApi({
  method: 'GET',
  url: '/project.json',
});
