import { snakeCase, camelCase } from 'change-case';

const isSnakeCase = str => str.includes('_');

const isCamelCase = str => /[A-Z]/.test(str) && str[0].toUpperCase !== str[0];

const isFirebaseId = key => key[0] === '-';

const convertPropNames = (transformMethod, checkMethod) => (data) => {
  const convert = convertPropNames(transformMethod, checkMethod);

  if (Array.isArray(data)) {
    return data.map(convert);
  }

  if (data && typeof data === 'object') {
    return Object.entries(data).reduce((newObj, [key, value]) => {
      if (typeof value === 'object') {
        // eslint-disable-next-line no-param-reassign
        newObj[key] = convert(value);
      } else if (!isFirebaseId(key) && checkMethod(key)) {
        const newKey = transformMethod(key);
        // eslint-disable-next-line no-param-reassign
        newObj[newKey] = value;
      } else {
        // eslint-disable-next-line no-param-reassign
        newObj[key] = value;
      }

      return newObj;
    }, {});
  }

  return data;
};

const objectKeysToSnakeCase = convertPropNames(snakeCase, isCamelCase);
const objectKeysToCamelCase = convertPropNames(camelCase, isSnakeCase);

export function onRequestInterceptor(request) {
  if (request.data) {
    request.data = objectKeysToSnakeCase(request.data);
  }

  return request;
}

export function onResponseInterceptor(response) {
  if (response.data) {
    response.data = objectKeysToCamelCase(response.data);
  }

  return response;
}
