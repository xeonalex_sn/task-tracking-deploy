import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import styledNormalize from 'styled-normalize';
import { createGlobalStyle } from 'styled-components';
import store from 'store';
import { Provider } from 'react-redux';

import App from 'components/App';

const GlobalStyle = createGlobalStyle`
  ${styledNormalize}
  
  * {
  box-sizing: border-box;
  }
  
  .ReactModal__Body--open {
    overflow: hidden;
  }
  
  #root {
    min-height: 100vh;
  }
`;

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <GlobalStyle />
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
);
