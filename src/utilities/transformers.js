/* eslint-disable import/prefer-default-export */
export function firebaseCollectionToArray(obj) {
  return Object.keys(obj).map(key => ({
    id: key,
    ...obj[key],
  }));
}
