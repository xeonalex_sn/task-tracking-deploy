import { getPriorities } from 'api';

export default {
  state: {
    priorities: null,
  },
  effects: {
    async getPriorities() {
      try {
        const { data } = await getPriorities();

        if (!data) throw new Error('No data (getPriorities)');

        this.setPriorities(data);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    },
  },
  reducers: {
    setPriorities(state, payload) {
      return {
        ...state,
        priorities: payload,
      };
    },
  },
};
