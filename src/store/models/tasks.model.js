import {
  createTask, getTasks, deleteTask, editTask,
} from 'api';
import { firebaseCollectionToArray } from 'utilities';

export default {
  state: {
    tasks: [],
    fetched: false,
    pending: false,
  },
  effects: {
    async getTasks(payload) {
      this.getTasksRequest();

      try {
        const { data } = await getTasks(payload);

        if (!data) throw new Error('No data (getTasks)');

        const newList = firebaseCollectionToArray(data);

        this.setTasks(newList);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        this.getTasksError();
      }
    },
    async createTask(payload) {
      try {
        const { data } = await createTask(payload);

        if (!data) throw new Error('No data (createTask)');

        const newTask = {
          ...payload,
          id: data.name,
        };

        this.addTask(newTask);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        return false;
      }
      return true;
    },
    async editTask(payload) {
      try {
        const { data } = await editTask(payload);

        if (!data) throw new Error('No data (editTask)');

        this.updateTask(payload);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        return false;
      }
      return true;
    },
    async deleteTaskRequest(id) {
      try {
        const { status } = await deleteTask(id);

        if (status !== 200) throw new Error('Tasks can\'t be deleted because of server error');

        this.deleteTask(id);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
      }
    },
  },
  reducers: {
    getTasksRequest(state) {
      return {
        ...state,
        pending: true,
      };
    },
    setTasks(state, payload) {
      return {
        ...state,
        tasks: payload,
        fetched: true,
        pending: false,
      };
    },
    getTasksError(state) {
      return {
        ...state,
        fetched: false,
        pending: false,
      };
    },
    addTask(state, payload) {
      return {
        ...state,
        tasks: [
          ...state.tasks,
          payload,
        ],
      };
    },
    updateTask(state, { id, data }) {
      const updatedTasksList = state.tasks.map((task) => {
        if (task.id === id) {
          return { ...task, ...data };
        }
        return task;
      });

      return {
        ...state,
        tasks: updatedTasksList,
      };
    },
    deleteTask(state, id) {
      const existingTasks = state.tasks.filter(task => task.id !== id);

      return {
        ...state,
        tasks: existingTasks,
      };
    },
  },
};
