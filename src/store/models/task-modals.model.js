export default {
  state: {
    editTaskData: null,
    deleteTaskId: null,
    completionTaskId: null,
  },
  effects: {
  },
  reducers: {
    setEditTaskData(state, payload) {
      return {
        ...state,
        editTaskData: payload,
      };
    },
    removeEditTaskData(state) {
      return {
        ...state,
        editTaskData: null,
      };
    },
    setDeleteTaskData(state, payload) {
      return {
        ...state,
        deleteTaskId: payload,
      };
    },
    removeDeleteTaskData(state) {
      return {
        ...state,
        deleteTaskId: null,
      };
    },
    setCompletionTaskData(state, payload) {
      return {
        ...state,
        completionTaskId: payload,
      };
    },
    removeCompletionTaskData(state) {
      return {
        ...state,
        completionTaskId: null,
      };
    },
  },
};
