import ReactModal from 'react-modal';
import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';

ReactModal.setAppElement('#root');

export const ModalBody = styled.div`
  background: #fff;
  border-radius: 4px;
  border: 1px solid #000;  
  width: 400px;
`;

export const ModalHeader = styled.div`
  background: lightgray;
  padding: 10px 30px;
`;

export const ModalTitle = styled.span`
  font-size: 1rem;
`;

export const ModalContent = styled.div`
  padding: 30px 30px;
`;

// The purpose of Adapter is to pass in
// Modal className, including generated from styled-component
const ModalAdapter = ({ className, ...props }) => (
  <ReactModal
    portalClassName={className}
    className={className}
    {...props}
  />
);

ModalAdapter.propTypes = {
  className: PropTypes.string.isRequired,
};

const Modal = styled(ModalAdapter).attrs(({ className }) => ({
  className: `modal ${className}`,
  overlayClassName: 'modal__overlay',
}))`
  .modal {
    position: absolute;
    top: 50%;
    left: 50%;
    right: auto;
    bottom: auto;
    margin-right: -50%;
    transform: translate(-50%, -50%);
    &:focus {
      outline: none;
    }
  }
  
  .modal__overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0.3);
  }
`;

export default Modal;
