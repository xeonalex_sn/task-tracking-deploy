import React from 'react';
import ReactDatePicker from 'react-datepicker';
import styled from 'styled-components';
import CalendarSVG from 'resources/images/icons/calendar.svg';

const InputWrapper = styled.div`
  width: 100%; 
  border: 1px solid #000;
  display: flex;
  justify-content: space-between;
`;

const ValueWrapper = styled.span`
  display: inline-block;
  padding: 10px;  
  font-size: 1rem;
`;

const IconWrapper = styled.div`
  display: block;
  border-left: 1px solid #000;
  padding: 10px;
  cursor: pointer;
`;

const CalendarIcon = styled(CalendarSVG)`
  height: 18px;
  display: block;
`;

// eslint-disable-next-line react/prop-types
const Input = React.forwardRef(({ onClick, value }, ref) => (
  <InputWrapper ref={ref}>
    <ValueWrapper>
      {value}
    </ValueWrapper>
    <IconWrapper onClick={onClick}>
      <CalendarIcon />
    </IconWrapper>
  </InputWrapper>
));

const StyledReactDatePicker = styled.div`

  .react-datepicker {
    font-size: 0.8rem;
    background-color: #fff;
    color: #000;
    border: 1px solid #aeaeae;
    border-radius: 0.3rem;
    display: inline-block;
    position: relative;
    
    &-popper {
      z-index: 1;
    }
    
    &__navigation {
      background: none;
      line-height: 1.7rem;
      text-align: center;
      cursor: pointer;
      position: absolute;
      top: 10px;
      width: 0;
      padding: 0;
      border: 0.45rem solid transparent;
      z-index: 1;
      height: 10px;
      width: 10px;
      text-indent: -999em;
      overflow: hidden;
    
      &--previous {
        left: 10px;
        border-right-color: #ccc;
        &:focus {
          outline: none;
        }
      }
    
      &--next {
        right: 10px;
        border-left-color: #ccc;
        &:focus {
          outline: none;
        }
      }
    }
  
    &__header {
      text-align: center;
      background-color: #f0f0f0;
      border-bottom: 1px solid #aeaeae;
      border-top-left-radius: 0.3rem;
      border-top-right-radius: 0.3rem;
      padding-top: 8px;
      position: relative;
    }
    
    &__current-month, 
    &-time__header, 
    &-year-header {
      margin-top: 0;
      color: #000;
      font-weight: bold;
      font-size: 0.944rem;
    }
    
    &__day-name, 
    &__day, 
    &__time-name {
      color: #000;
      display: inline-block;
      width: 1.7rem;
      line-height: 1.7rem;
      text-align: center;
      margin: 0.166rem;
    }
    
    &__day--selected {
      border-radius: 0.3rem;
      background-color: #216ba5;
      color: #fff;
    }
    
    &__day, 
    &__month-text {
        cursor: pointer;
    }
    
    &__month {
      margin: 0.4rem;
      text-align: center;
    }
    
    &__day-names, 
    &__week {
      white-space: nowrap;
    }
    
    &__day--selected {
      border-radius: 0.3rem;
      background-color: #216ba5;
      color: #fff;
    }
  
    &__day:hover, 
    &__month-text:hover {
      border-radius: 0.3rem;
      background-color: #f0f0f0;
    }
    
    &-wrapper,
    &__input-container{
      display: block;
    }
    
    &-popper[data-placement^="bottom"] {
      margin-top: 10px;
    }
  }
`;

const Datepicker = props => (
  <StyledReactDatePicker>
    <ReactDatePicker
      customInput={<Input />}
      {...props}
    />
  </StyledReactDatePicker>
);


export default Datepicker;
