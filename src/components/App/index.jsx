import React from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { ROUTES_URLS } from 'constants';
import styled from 'styled-components';

import Header from './Main/Header';
import Menu from './Main/Menu';
import Today from './Main/Today';
import Analytic from './Main/Analytic';

const ContentContainer = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding:  100px 30px
`;

const App = () => (
  <ContentContainer>
    <Header />
    <Menu />
    <Switch>
      <Route path={ROUTES_URLS.today} component={Today} />
      <Route path={ROUTES_URLS.analytic} component={Analytic} />

      <Redirect from="/" to={ROUTES_URLS.today} />
    </Switch>
  </ContentContainer>
);

export default App;
