import React from 'react';
import styled from 'styled-components';

const ContentContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 40px;
`;

const HeaderButtonsWrapper = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;

export const SettingsMenuButton = styled.button`
  color: #000;
  transition: background .3s;
  font-size: 18px;
  width: 30px;
  height: 30px;
  margin-right: 15px;
  padding: 0px 5px 10px;
  border-radius: 50%;
  background: rgba(0,0,0,0);
  border: none;
  cursor: pointer;
  
  :focus {
    outline: none;
  }
  :hover {
    background-color: lightgrey
  }
`;

const UserAvatar = styled.div`
  transition: background .3s;
  width: 50px;
  height: 50px; 
  border-radius: 50%;
  background: blue;
  overflow:hidden;
  border: 2px solid black;
  cursor: pointer;
  font-size: 26px;
  line-height: 1;
  padding: 12px 0;
  text-align: center;

  :hover {
    background-color: #0000e2;
  }
`;

const Header = () => (
  <ContentContainer>
    <HeaderButtonsWrapper>
      <SettingsMenuButton>...</SettingsMenuButton>
      <UserAvatar>P</UserAvatar>
    </HeaderButtonsWrapper>
  </ContentContainer>
);

export default Header;
