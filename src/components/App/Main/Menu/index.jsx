import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import { ROUTES_URLS } from 'constants';

const MainNavWrapper = styled.div`
   display: flex;
   align-items: center;
   border: 1px solid #000;
   border-radius: 25px;
   overflow: hidden;
   width: 300px;
   margin: 0 auto 40px;
`;

const NavLinkCustom = styled(NavLink)`
  display: block;
  background: rgba(0,0,0,0);
  color: #000;
  text-decoration: none;
  transition: background-color .3s;
  border-right: 1px solid #000;
  width: 50%;
  padding: 10px 15px;
  text-align: center;
  
  &:last-child {
    border-right: none;
  }
  &:hover {
    background: lightgray;
  }
  
  &.active {
    background: lightgray;
  }
`;


const Menu = () => (
  <MainNavWrapper>
    <NavLinkCustom to={ROUTES_URLS.today} activeClassName="active">
      Today
    </NavLinkCustom>
    <NavLinkCustom to={ROUTES_URLS.analytic} activeClassName="active">
      Analytics
    </NavLinkCustom>
  </MainNavWrapper>
);

export default Menu;
