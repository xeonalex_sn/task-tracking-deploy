import React from 'react';
import styled from 'styled-components';

const AnalyticWrapper = styled.div`
  text-align: center;
`;


const Analytic = () => (
  <AnalyticWrapper>
    AnalyticView
  </AnalyticWrapper>
);

export default Analytic;
