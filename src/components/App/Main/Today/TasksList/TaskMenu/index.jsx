import React from 'react';
import styled from 'styled-components';
import { MenuProvider, Menu } from 'react-contexify';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { taskExtendedShape } from 'shapes';

export const SettingsMenuButton = styled(MenuProvider)`
  position: absolute;
  top: 30px;
  color: #000;
  transition: background .3s;
  font-size: 18px;
  width: 30px;
  height: 30px;
  padding: 0 5px 10px;
  border-radius: 50%;
  background: rgba(0,0,0,0);
  text-align: center;
  border: none;
  cursor: pointer;
  transform: rotate(-180deg);

  :focus {
    outline: none;
  }
  :hover {
    background-color: lightgrey
  }
`;

const DropMenuWrapper = styled(Menu)`
    position: fixed;
    opacity: 0;
    user-select: none;
    background-color: #ffffff;
    box-sizing: border-box;
    box-shadow: rgba(0, 0, 0, .3) 0 10px 20px, rgb(238, 238, 238) 0 0 0 1px;
    padding: 5px 0;
    min-width: 180px;
`;

const DropMenuLink = styled.a`
  display: block;
  padding: 10px;
  background: rgba(0,0,0,0);
  transition: background .3s;

  &:hover {
    background: rgb(128,128,128,0.5);
  }
`;

const TaskMenu = ({ task, openEditTaskModal, openDeleteTaskModal }) => (
  <div>
    <SettingsMenuButton event="onClick" id={task.id}>...</SettingsMenuButton>
    <DropMenuWrapper id={task.id}>
      <DropMenuLink onClick={() => openEditTaskModal(task)}> Edit </DropMenuLink>
      <DropMenuLink onClick={() => openDeleteTaskModal(task.id)}> Delete </DropMenuLink>
    </DropMenuWrapper>
  </div>
);

TaskMenu.propTypes = {
  task: taskExtendedShape.isRequired,
  openEditTaskModal: PropTypes.func.isRequired,
  openDeleteTaskModal: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  openEditTaskModal: dispatch.taskModals.setEditTaskData,
  openDeleteTaskModal: dispatch.taskModals.setDeleteTaskData,
});

export default connect(null, mapDispatchToProps)(TaskMenu);
