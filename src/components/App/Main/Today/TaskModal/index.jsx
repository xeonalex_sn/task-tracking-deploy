import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import {
  func, shape, string, number,
} from 'prop-types';
import { Button, Datepicker, Select } from 'components/UI';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { selectOptionShape, selectOptionsShape } from 'shapes';

const FormRow = styled.div`
  margin-bottom: 20px;
`;

const FormError = styled.div`
  color: indianred;
  font-size: 0.875rem;
  margin-top: 5px;
`;

const Input = styled.input`
  display: block;
  width: 100%;
  padding: 8px 10px;
  border: 1px solid #000;
  &:focus {
    outline: none;
  }
  &::placeholder {
    color: #000;
  }
`;

const FormButtons = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const ButtonSubmit = styled(Button)`
  margin-right: 10px;
`;

const TaskModal = ({
  actions, projects, priorities, closeHandler, submitBtnText, handleSubmit, initialValues,
}) => {
  const [name, setName] = useState(initialValues.name || '');
  const [project, setProject] = useState(initialValues.project || null);
  const [priority, setPriority] = useState(initialValues.priority || null);
  const [date, setDate] = useState(initialValues.date || Date.now());
  const [errors, setErrors] = useState({});

  useEffect(() => {
    actions.getPriorities();
    actions.getProjects();
  }, []);

  function validateForm() {
    const newErrors = {};

    if (!name) {
      newErrors.name = 'Task name is required';
    }
    if (!project) {
      newErrors.project = 'Project is required';
    }
    if (!priority) {
      newErrors.priority = 'Task priority is required';
    }
    if (!date) {
      newErrors.date = 'Date is required';
    }

    setErrors(newErrors);

    return !Object.keys(newErrors).length;
  }

  const onSubmit = () => {
    if (validateForm()) {
      handleSubmit({
        date,
        project,
        priority,
        name,
      });
    }
  };

  return (
    <form>
      <FormRow>
        <Input
          type="text"
          placeholder="Name"
          value={name}
          onChange={e => setName(e.target.value)}
        />
        { errors.name && (<FormError>{errors.name}</FormError>) }
      </FormRow>
      <FormRow>
        <Select
          options={projects}
          placeholder="Project"
          onChange={option => setProject(option)}
          value={project}
        />
        { errors.project && (<FormError>{errors.project}</FormError>) }
      </FormRow>
      <FormRow>
        <Datepicker
          selected={date}
          onChange={newDate => setDate(+newDate)}
          dateFormat="dd MMM yyyy"
        />
        { errors.date && (<FormError>{errors.date}</FormError>) }
      </FormRow>
      <FormRow>
        <Select
          options={priorities}
          placeholder="Priority"
          onChange={option => setPriority(option)}
          value={priority}
        />
        { errors.priority && (<FormError>{errors.priority}</FormError>) }
      </FormRow>

      <FormButtons>
        <ButtonSubmit primary onClick={onSubmit}>{submitBtnText}</ButtonSubmit>
        <Button onClick={closeHandler}> Cancel</Button>
      </FormButtons>
    </form>
  );
};

TaskModal.propTypes = {
  actions: shape({
    getPriorities: func.isRequired,
    getProjects: func.isRequired,
  }).isRequired,
  initialValues: shape({
    date: number,
    name: string,
    project: selectOptionShape,
    priority: selectOptionShape,
  }),
  projects: selectOptionsShape,
  priorities: selectOptionsShape,
  closeHandler: func.isRequired,
  submitBtnText: string.isRequired,
  handleSubmit: func.isRequired,
};

TaskModal.defaultProps = {
  projects: [],
  priorities: [],
  initialValues: {},
};

function transformSelectData(dataObj) {
  return dataObj && Object.keys(dataObj).map(key => ({
    value: key,
    label: dataObj[key].name,
  }));
}

const getSelectProjects = createSelector(
  ({ projects }) => projects.projects,
  transformSelectData,
);

const getSelectPriorities = createSelector(
  ({ priorities }) => priorities.priorities,
  transformSelectData,
);

const mapDispatchToProps = ({ tasks, priorities, projects }) => ({
  actions: {
    createTask: tasks.createTask,
    getPriorities: priorities.getPriorities,
    getProjects: projects.getProjects,
  },
});

const mapStateToProps = state => ({
  projects: getSelectProjects(state),
  priorities: getSelectPriorities(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskModal);
