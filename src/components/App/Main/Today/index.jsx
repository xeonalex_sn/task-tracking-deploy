import React, { useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { func, shape, string } from 'prop-types';
import DateTime from 'components/App/Main/Today/DateTime';
import CreateTaskButton from 'components/App/Main/Today/CreateTaskButton';
import CreateTaskModal from 'components/App/Main/Today/CreateTaskModal';
import TasksList from 'components/App/Main/Today/TasksList';
import DeleteTaskModal from 'components/App/Main/Today/DeleteTaskModal';
import EditTaskModal from 'components/App/Main/Today/EditTaskModal';
import { taskExtendedShape } from 'shapes';
import CompletionTaskModal from 'components/App/Main/Today/CompletionTaskModal';

const TodayWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const Today = ({
  actions, editTaskData, deleteTaskId, completionTaskId,
}) => {
  const [isCreateTaskModalOpen, setIsCreateTaskModalOpen] = useState(false);

  return (
    <TodayWrapper>
      <DateTime />
      <TasksList />

      <CreateTaskButton clickHandler={() => setIsCreateTaskModalOpen(true)} />
      <CreateTaskModal
        isOpen={isCreateTaskModalOpen}
        closeHandler={() => setIsCreateTaskModalOpen(false)}
      />
      <DeleteTaskModal
        taskId={deleteTaskId}
        closeHandler={actions.closeDeleteTaskModal}
      />
      <EditTaskModal
        taskData={editTaskData}
        closeHandler={actions.closeEditTaskModal}
      />
      <CompletionTaskModal
        taskId={completionTaskId}
        closeHandler={actions.closeCompletionTaskModal}
      />
    </TodayWrapper>
  );
};

Today.propTypes = {
  editTaskData: taskExtendedShape,
  deleteTaskId: string,
  completionTaskId: string,
  actions: shape({
    closeEditTaskModal: func.isRequired,
    closeDeleteTaskModal: func.isRequired,
  }).isRequired,
};

Today.defaultProps = {
  editTaskData: null,
  deleteTaskId: null,
  completionTaskId: null,
};

const mapStateToProps = ({ taskModals }) => ({
  editTaskData: taskModals.editTaskData,
  deleteTaskId: taskModals.deleteTaskId,
  completionTaskId: taskModals.completionTaskId,
});

const mapDispatchToProps = dispatch => ({
  actions: {
    closeEditTaskModal: dispatch.taskModals.removeEditTaskData,
    closeDeleteTaskModal: dispatch.taskModals.removeDeleteTaskData,
    closeCompletionTaskModal: dispatch.taskModals.removeCompletionTaskData,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Today);
