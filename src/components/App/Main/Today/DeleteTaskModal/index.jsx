import React from 'react';
import styled from 'styled-components';
import { func, shape, string } from 'prop-types';
import {
  Modal, Button, ModalBody, ModalContent, ModalHeader, ModalTitle,
} from 'components/UI';
import { connect } from 'react-redux';

const ControlsWrap = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const ButtonSubmit = styled(Button)`
  margin-right: 10px;
`;

const DeleteTaskModal = ({
  actions, closeHandler, taskId,
}) => {
  function onApprove() {
    actions.deleteTask(taskId).then(closeHandler);
  }

  return (
    <Modal
      isOpen={!!taskId}
      onRequestClose={closeHandler}
    >
      <ModalBody>
        <ModalHeader>
          <ModalTitle>Delete task</ModalTitle>
        </ModalHeader>
        <ModalContent>
          <ControlsWrap>
            <ButtonSubmit primary onClick={onApprove}>Confirm </ButtonSubmit>
            <Button onClick={closeHandler}> Cancel</Button>
          </ControlsWrap>
        </ModalContent>
      </ModalBody>
    </Modal>
  );
};

DeleteTaskModal.propTypes = {
  closeHandler: func.isRequired,
  actions: shape({
    deleteTask: func.isRequired,
  }).isRequired,
  taskId: string,
};

DeleteTaskModal.defaultProps = {
  taskId: null,
};

const mapDispatchToProps = ({ tasks }) => ({
  actions: {
    deleteTask: tasks.deleteTaskRequest,
  },
});

export default connect(null, mapDispatchToProps)(DeleteTaskModal);
