import React from 'react';
import { bool, func, shape } from 'prop-types';
import {
  Modal, ModalBody, ModalContent, ModalHeader, ModalTitle,
} from 'components/UI';
import { connect } from 'react-redux';
import TaskModal from 'components/App/Main/Today/TaskModal';

const CreateTaskModal = ({
  actions, isOpen, closeHandler,
}) => {
  const handleSubmit = ({
    name, priority, project, date,
  }) => {
    actions.createTask({
      name: name.trim(),
      priorityId: priority.value,
      projectId: project.value,
      date,
      time: null,
      done: false,
    })
      .then(closeHandler);
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={closeHandler}
    >
      <ModalBody>
        <ModalHeader>
          <ModalTitle>Create task</ModalTitle>
        </ModalHeader>
        <ModalContent>
          {
            isOpen
            && (
              <TaskModal
                closeHandler={closeHandler}
                submitBtnText="Create"
                handleSubmit={handleSubmit}
              />
            )
          }
        </ModalContent>
      </ModalBody>
    </Modal>
  );
};

CreateTaskModal.propTypes = {
  isOpen: bool.isRequired,
  closeHandler: func.isRequired,
  actions: shape({
    createTask: func.isRequired,
  }).isRequired,
};

const mapDispatchToProps = ({ tasks }) => ({
  actions: {
    createTask: tasks.createTask,
  },
});

export default connect(null, mapDispatchToProps)(CreateTaskModal);
