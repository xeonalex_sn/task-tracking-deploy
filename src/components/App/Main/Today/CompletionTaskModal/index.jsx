import React, { useState } from 'react';
import styled from 'styled-components';
import { func, string } from 'prop-types';
import {
  Modal, Button, ModalBody, ModalContent, ModalHeader, ModalTitle,
} from 'components/UI';
import { connect } from 'react-redux';

const FormRow = styled.div`
  margin-bottom: 20px;
`;

const FormError = styled.div`
  color: indianred;
  font-size: 0.875rem;
  margin-top: 5px;
`;

const Input = styled.input`
  display: block;
  width: 100%;
  padding: 8px 10px;
  border: 1px solid #000;
  &:focus {
    outline: none;
  }
  &::placeholder {
    color: #000;
  }
`;

const FormButtons = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const ButtonSubmit = styled(Button)`
  margin-right: 10px;
`;

const MIN_TIME = 5;

const CompletionTaskModal = ({
  editTask, closeHandler, taskId,
}) => {
  const [time, setTime] = useState(MIN_TIME);
  const [timeError, setTimeError] = useState('');

  function validateForm() {
    let newErrors = '';

    if (!time) {
      newErrors = 'Task name is required';
    }

    setTimeError(newErrors);

    return !newErrors;
  }

  const onSubmit = () => {
    if (validateForm()) {
      editTask({
        id: taskId,
        data: {
          done: true,
          time,
        },
      }).then(closeHandler);
    }
  };

  return (
    <Modal
      isOpen={!!taskId}
      onRequestClose={closeHandler}
    >
      <ModalBody>
        <ModalHeader>
          <ModalTitle>Task time settings</ModalTitle>
        </ModalHeader>
        <ModalContent>
          <form>
            <FormRow>
              <Input
                type="number"
                min={MIN_TIME}
                placeholder="minutes"
                value={time}
                onChange={e => setTime(+e.target.value)}
              />
              { timeError && (<FormError>{timeError}</FormError>) }
            </FormRow>
            <FormButtons>
              <ButtonSubmit primary onClick={onSubmit}>Done</ButtonSubmit>
              <Button onClick={closeHandler}> Cancel</Button>
            </FormButtons>
          </form>
        </ModalContent>
      </ModalBody>
    </Modal>
  );
};

CompletionTaskModal.propTypes = {
  closeHandler: func.isRequired,
  editTask: func.isRequired,
  taskId: string,
};

CompletionTaskModal.defaultProps = {
  taskId: null,
};

const mapDispatchToProps = ({ tasks }) => ({
  editTask: tasks.editTask,
});

export default connect(null, mapDispatchToProps)(CompletionTaskModal);
