import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BtnWrapper = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 2rem;
  cursor: pointer;
  bottom: 50px;
  right: 50px;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background: blue;
  transition: transform .3s;
  color: #fff;
  
  :hover {
    transform: scale(1.1);
  }
`;

const CreateTaskButton = ({ clickHandler }) => (
  <BtnWrapper onClick={clickHandler}>
      +
  </BtnWrapper>
);

CreateTaskButton.propTypes = {
  clickHandler: PropTypes.func.isRequired,
};

export default CreateTaskButton;
