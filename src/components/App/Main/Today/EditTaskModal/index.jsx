import React from 'react';
import { func, shape } from 'prop-types';
import {
  Modal, ModalTitle, ModalHeader, ModalContent, ModalBody,
} from 'components/UI';
import { connect } from 'react-redux';
import TaskModal from 'components/App/Main/Today/TaskModal';
import { taskExtendedShape } from 'shapes';

const EditTaskModal = ({
  actions, closeHandler, taskData,
}) => {
  const handleSubmit = ({
    name, priority, project, date,
  }) => {
    actions.editTask({
      id: taskData.id,
      data: {
        name: name.trim(),
        priorityId: priority.value,
        projectId: project.value,
        date,
      },
    })
      .then(closeHandler);
  };

  return (
    <Modal
      isOpen={!!taskData}
      onRequestClose={closeHandler}
    >
      <ModalBody>
        <ModalHeader>
          <ModalTitle>Edit task</ModalTitle>
        </ModalHeader>
        <ModalContent>
          {
            taskData
            && (
              <TaskModal
                initialValues={taskData}
                closeHandler={closeHandler}
                handleSubmit={handleSubmit}
                submitBtnText="Edit"
              />
            )
          }
        </ModalContent>
      </ModalBody>
    </Modal>
  );
};

EditTaskModal.propTypes = {
  taskData: taskExtendedShape,
  closeHandler: func.isRequired,
  actions: shape({
    editTask: func.isRequired,
  }).isRequired,
};

EditTaskModal.defaultProps = {
  taskData: null,
};

const mapDispatchToProps = ({ tasks }) => ({
  actions: {
    editTask: tasks.editTask,
  },
});

export default connect(null, mapDispatchToProps)(EditTaskModal);
